package jbayangosb.talktalk.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DatabaseReference
import com.onesignal.OneSignal
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import jbayangosb.talktalk.R
import jbayangosb.talktalk.controller.FirebaseHandler
import jbayangosb.talktalk.controller.NetworkHandler
import jbayangosb.talktalk.model.Model
import jbayangosb.talktalk.model.User
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    private val firebaseAuth = FirebaseAuth.getInstance()
    private val authListener = FirebaseHandler.initAuthListener()
    private val onlineUsers: ArrayList<User> = arrayListOf()
    private var userAdapter: UserAdapter? = null
    private val TAG = MainActivity::class.java.simpleName

    // Firebase reference and listener
    private var databaseReference: DatabaseReference? = null
    private var childListener: ChildEventListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Init user list
        userAdapter = UserAdapter(onlineUsers)
        initOnUserClick()
        initRefreshButton()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = userAdapter
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(authListener)
    }

    override fun onResume() {
        super.onResume()
        checkNetwork()
    }

    private fun checkNetwork() {
        if (!NetworkHandler.isOnline(this)) {
            toast("Please check your connection")
            hideProgressBar()
            showRefreshButton()
        } else {
            signInDevice()
        }
    }

    override fun onStop() {
        firebaseAuth.removeAuthStateListener(authListener)
        super.onStop()
    }

    private fun signInDevice() {
        firebaseAuth.signInAnonymously().addOnCompleteListener(object : OnCompleteListener<AuthResult> {
            override fun onComplete(task: Task<AuthResult>) {
                if (task.isSuccessful) {
                    toast("Successful login")
                    notifyDatabase()
                } else {
                    hideProgressBar()
                    toast("Something went wrong")
                }
            }
        })
    }

    private fun initRefreshButton() {
        btnRefresh.setOnClickListener {
            showProgressBar()
            hideRefreshButton()
            checkNetwork()
        }
    }

    // Add user to firebase list
    private fun notifyDatabase() {
        onlineUsers.clear()
        userAdapter!!.notifyDataSetChanged()
        val successSubject = PublishSubject.create<Unit>()
        successSubject.subscribeActual(object: Observer<Unit> {
            override fun onError(e: Throwable?) {
            }

            override fun onSubscribe(d: Disposable?) {
            }

            override fun onNext(t: Unit?) {
            }

            override fun onComplete() {
                toast("You are now online!")
                getOnlineUsers()
            }
        })
        FirebaseHandler.notifyDatabaseAboutUser(successSubject)
    }

    private fun getOnlineUsers() {
        FirebaseHandler.getOnlineUsers(this)
    }

    // Add users to list unless they are the device user
    fun addUserToList(user: User) {
        hideProgressBar()
        Log.i(TAG, "User: "+ user.uid)
        if (user.uid == Model.user!!.uid) {
            Log.i(TAG, "Model: " + Model.user!!.uid)
            return
        }
        onlineUsers.add(user)
        userAdapter!!.notifyDataSetChanged()
    }

    fun removeUserFromList(user: User) {
        onlineUsers.remove(user)
        userAdapter!!.notifyDataSetChanged()
    }

    fun storeDatabaseReference(ref: DatabaseReference, listener: ChildEventListener) {
        databaseReference = ref
        childListener = listener
    }

    override fun onPause() {
        // Remove child event listener when no longer in use
        if (databaseReference != null) {
            databaseReference!!.removeEventListener(childListener)
        }
        super.onPause()
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    private fun hideRefreshButton() {
        btnRefresh.visibility = View.GONE
    }

    private fun showRefreshButton() {
        btnRefresh.visibility = View.VISIBLE
    }

    private fun initOnUserClick() {
        userAdapter!!.onClickSubject.subscribeActual(object : Observer<User> {
            override fun onNext(user: User?) {
                // Start conversation activity with selected user
                Model.selectedUser = user
                startActivity(Intent(this@MainActivity, ConversationActivity::class.java))
            }

            override fun onSubscribe(d: Disposable?) {
            }

            override fun onError(e: Throwable?) {

            }

            override fun onComplete() {

            }
        })
    }
}
