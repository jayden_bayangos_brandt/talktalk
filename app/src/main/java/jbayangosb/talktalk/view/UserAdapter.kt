package jbayangosb.talktalk.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.reactivex.subjects.PublishSubject
import jbayangosb.talktalk.R
import jbayangosb.talktalk.model.User
import kotlinx.android.synthetic.main.item_user.view.*
import org.jetbrains.anko.find

class UserAdapter(var users: ArrayList<User>) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    val onClickSubject = PublishSubject.create<User>()

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var txtName: TextView
        init {
            txtName = itemView.find(R.id.txtName)
        }
    }

    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        val user = users.get(position)
        holder.itemView.txtName.text = user.uid
        holder.itemView.setOnClickListener {
            onClickSubject.onNext(user)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent!!.context)
        val view = layoutInflater.inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }
}
