package jbayangosb.talktalk.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

import jbayangosb.talktalk.R
import jbayangosb.talktalk.controller.FirebaseHandler
import jbayangosb.talktalk.controller.NetworkHandler
import jbayangosb.talktalk.model.Conversation
import jbayangosb.talktalk.model.Message
import jbayangosb.talktalk.model.Model
import kotlinx.android.synthetic.main.activity_conversation.*
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class ConversationActivity : AppCompatActivity() {

    private var messageAdapter: MessageAdapter? = null
    private var conversation: Conversation? = null
    private val messages = linkedMapOf<String, Message>()
    private val TAG = ConversationActivity::class.java.simpleName

    // Message listener
    private var messageListener: ChildEventListener? = null
    private var messageReference: DatabaseReference? = null

    // Typing listener
    private var typingListener: ValueEventListener? = null
    private var typingReference: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conversation)
        // Init message layout
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        messageAdapter = MessageAdapter(messages)
        recyclerView.adapter = messageAdapter
        initRefreshButton()
    }

    override fun onResume() {
        super.onResume()
        checkNetwork()

    }

    private fun checkNetwork() {
        if (!NetworkHandler.isOnline(this)) {
            toast("Please check your connection")
            hideProgressBar()
            showRefreshButton()
        } else {
            getConversation()
        }
    }

    private fun hideRefreshButton() {
        btnRefresh.visibility = View.GONE
    }

    private fun showRefreshButton() {
        btnRefresh.visibility = View.VISIBLE
        hideNoMessageText()
    }

    private fun hideNoMessageText() {
        txtNoMessages.visibility = View.GONE
    }

    private fun showNoMessageText() {
        txtNoMessages.visibility = View.VISIBLE
    }

    private fun initRefreshButton() {
        btnRefresh.setOnClickListener {
            showProgressBar()
            hideRefreshButton()
            checkNetwork()
        }
    }

    private fun initTypingListener() {
        fldMessage.addTextChangedListener(object : TextWatcher {
            var timer = Timer()
            val delay: Long = 5000
            var isTyping = false
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                // When user typing, change typing status in firebase
                if(!isTyping) {
                    Log.i(TAG, "Started typing")
                    FirebaseHandler.setTypingStatus(conversation!!, Model.user!!.uid, true)
                    isTyping = true
                }
                // Set timer for 5 seconds after user has stopped typing before
                // changing typing status back to false
                timer.cancel()
                timer = Timer()
                timer.schedule(object: TimerTask() {
                    override fun run() {
                        isTyping = false
                        Log.i(TAG, "Stopped typing")
                        FirebaseHandler.setTypingStatus(conversation!!, Model.user!!.uid, false)
                    }
                }, delay)
            }
        })
    }

    private fun initCheckUserIsTyping() {
        val typingSubject = PublishSubject.create<Boolean>()
        typingSubject.subscribeActual(object : Observer<Boolean> {
            override fun onError(e: Throwable?) {
            }

            override fun onComplete() {
            }

            override fun onNext(isTyping: Boolean?) {
                // Show typing text depending on status
                if (isTyping!!) {
                    txtUserTyping.visibility = View.VISIBLE
                } else {
                    txtUserTyping.visibility = View.GONE
                }
            }

            override fun onSubscribe(d: Disposable?) {
            }
        })
        FirebaseHandler.getTypingStatus(conversation!!, Model.user!!.uid, typingSubject, this)
    }

    private fun getConversation() {
        val conversationSubject = PublishSubject.create<Conversation>()
        conversationSubject.subscribeActual(object: Observer<Conversation> {
            override fun onComplete() {
                hideProgressBar()
                if (conversation != null) {
                    Log.i(TAG, "Conversation has been found")
                } else {
                    Log.i(TAG, "Issue finding conversation")
                }
            }

            override fun onSubscribe(d: Disposable?) {

            }

            // Once conversation has been found, initilise various listeners
            // and get messages
            override fun onNext(convo: Conversation?) {
                conversation = convo
                initTypingListener()
                initCheckUserIsTyping()
                initSendingMessages()
                getMessages(convo!!)
            }

            override fun onError(e: Throwable?) {
                Log.i(TAG, "1 found")
            }
        })
        val uid = Model.user!!.uid
        val clickedUid = Model.selectedUser!!.uid
        FirebaseHandler.getConversation(uid, clickedUid, conversationSubject)
    }

    private fun getMessages(conversation: Conversation) {
        val messagesSubject = PublishSubject.create<Message>()
        messagesSubject.subscribeActual(object: Observer<Message> {
            override fun onSubscribe(d: Disposable?) {

            }

            // Show messages and scroll view
            override fun onNext(message: Message?) {
                hideNoMessageText()
                messages.put(message!!.key, message)
                messageAdapter!!.notifyDataSetChanged()
                recyclerView.scrollToPosition(messageAdapter!!.itemCount-1)

            }

            override fun onComplete() {
            }

            override fun onError(e: Throwable?) {

            }
        })
        Log.i(TAG, "Getting messages")
        FirebaseHandler.getMessages(conversation, messagesSubject, this)
    }

    fun storeFirebaseMessageListener(messageListener: ChildEventListener, messageReference: DatabaseReference) {
        this.messageListener = messageListener
        this.messageReference = messageReference
    }

    fun storeFirebaseTypingListener(typingListener: ValueEventListener, typingReference: DatabaseReference) {
        this.typingListener = typingListener
        this.typingReference = typingReference
    }

    override fun onPause() {
        if (messageReference != null && typingReference != null) {
            messageReference!!.removeEventListener(messageListener)
            typingReference!!.removeEventListener(typingListener)
        }
        super.onPause()
    }

    private fun initSendingMessages() {
        val messageSubject = PublishSubject.create<Boolean>()
        messageSubject.subscribeActual(object : Observer<Boolean> {
            override fun onSubscribe(d: Disposable?) {
            }

            override fun onComplete() {
            }

            override fun onNext(t: Boolean?) {
                if (t!!) {
                    toast("Message sent")
                } else {
                    toast("Sending failed!")
                }
            }

            override fun onError(e: Throwable?) {
            }
        })
        // Send message
        btnSend.setOnClickListener {
            if(!NetworkHandler.isOnline(this)) {
                toast("Please check your connection")
                return@setOnClickListener
            }
            if (fldMessage.text.isEmpty()) {
                toast("Please enter a message")
                return@setOnClickListener
            }
            val timeStamp = getTimeStamp()
            val text = fldMessage.text.toString()
            val sender = Model.user!!.uid
            val message = Message(timeStamp, text, sender)
            FirebaseHandler.sendMessage(conversation!!, message, messageSubject)
            fldMessage.text.clear()
        }
    }

    private fun getTimeStamp(): Long {
        val calendar = Calendar.getInstance()
        return calendar.timeInMillis
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

}
