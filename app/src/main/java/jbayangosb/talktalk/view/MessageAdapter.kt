package jbayangosb.talktalk.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import jbayangosb.talktalk.R
import jbayangosb.talktalk.model.Message
import jbayangosb.talktalk.model.Model
import kotlinx.android.synthetic.main.item_message.view.*
import org.jetbrains.anko.find
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.LinkedHashMap

class MessageAdapter(var messages: LinkedHashMap<String, Message>) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var txtSender: TextView
        var txtMessage: TextView
        var txtTimeStamp: TextView
        var txtStatus: TextView
        init {
            txtSender = itemView.find(R.id.txtSender)
            txtMessage = itemView.find(R.id.txtMessage)
            txtTimeStamp = itemView.find(R.id.txtTimeStamp)
            txtStatus = itemView.find(R.id.txtStatus)
        }
    }

    override fun onBindViewHolder(holder: MessageAdapter.ViewHolder, position: Int) {
        val keys = messages.keys
        val message = messages.get(keys.elementAt(position))!!
        // Check if message is from user
        if (message.sender == Model.user!!.uid) {
            // Show message status if confirmed to be user
            holder.itemView.txtSender.text = "Me:"
            holder.itemView.txtStatus.text = message.status
        } else {
            // Ignore message status if not user
            holder.itemView.txtSender.text = "Your friend:"
            holder.itemView.txtStatus.text = ""
        }
        holder.itemView.txtMessage.text = message.text
        // Setup timestamp for message
        val time = SimpleDateFormat("hh:mm")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = message.date
        val timeStamp = time.format(calendar.time)
        holder.itemView.txtTimeStamp.text = timeStamp
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent!!.context)
        val view = layoutInflater.inflate(R.layout.item_message, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return messages.size
    }
}
