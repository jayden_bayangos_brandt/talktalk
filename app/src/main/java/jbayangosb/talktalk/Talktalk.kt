package jbayangosb.talktalk

import android.app.Application
import com.onesignal.OneSignal

class Talktalk : Application {
    constructor() : super()

    override fun onCreate() {
        super.onCreate()
        OneSignal.startInit(this).init()
    }
}
