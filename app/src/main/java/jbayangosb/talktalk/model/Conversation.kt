package jbayangosb.talktalk.model

import com.google.firebase.database.Exclude

class Conversation(val creatorUid: String, val recipientUid: String, val messageKey: String) {
    constructor():this("", "", "")
    var creatorTyping: Boolean = false
    var recipientTyping: Boolean = false
    var creatorOneSignalId: String = ""
    var recipientOneSignalId: String = ""

    @get: Exclude
    var key: String = ""
}
