package jbayangosb.talktalk.model

import com.google.firebase.database.Exclude

class Message(val date: Long, val text: String, val sender: String) {
    constructor():this(0, "", "")
    var status: String = "Sent"

    @get: Exclude
    var key: String = ""
}
