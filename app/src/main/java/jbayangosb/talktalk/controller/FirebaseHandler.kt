package jbayangosb.talktalk.controller

import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.onesignal.OneSignal
import io.reactivex.subjects.PublishSubject
import jbayangosb.talktalk.model.Conversation
import jbayangosb.talktalk.model.Message
import jbayangosb.talktalk.model.Model
import jbayangosb.talktalk.model.User
import jbayangosb.talktalk.view.ConversationActivity
import jbayangosb.talktalk.view.MainActivity

object FirebaseHandler {
    val TAG = FirebaseHandler::class.java.simpleName
    val firebaseDatabase = FirebaseDatabase.getInstance()

    // Database constants
    val ONLINE_USERS = "OnlineUsers"
    val CONVERSATIONS = "Conversations"
    val MESSAGES = "Messages"

    // Check if user has been signed in
    fun initAuthListener(): FirebaseAuth.AuthStateListener {
        val listen = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                Log.i(TAG, "User signed in: "+user.uid)
                Model.user = User(user.uid)
                getOneSignalId()
            } else {
                Log.i(TAG, "User not signed in")
            }
        }
        return listen
    }

    private fun getOneSignalId() {
        OneSignal.idsAvailable { userId, registrationId ->
            Model.user!!.onesignalUid = userId
        }
    }

    // Add user to firebase
    fun notifyDatabaseAboutUser(successSubject: PublishSubject<Unit>) {
        val ref = firebaseDatabase.reference.child(ONLINE_USERS)

        ref.child(Model.user!!.uid).setValue(Model.user).addOnCompleteListener(object: OnCompleteListener<Void> {
            override fun onComplete(p0: Task<Void>) {
                successSubject.onComplete()
            }
        })
    }

    fun getOnlineUsers(mainActivity: MainActivity) {
        val ref = firebaseDatabase.reference.child(ONLINE_USERS)
        val listener = object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
            }

            // Add user to list
            override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
                val user = snapshot!!.getValue(User::class.java)
                mainActivity.addUserToList(user)
            }

            // Remove user from list
            override fun onChildRemoved(snapshot: DataSnapshot?) {
                val user = snapshot!!.getValue(User::class.java)
                mainActivity.removeUserFromList(user)
            }
        }
        ref.addChildEventListener(listener)
        mainActivity.storeDatabaseReference(ref, listener)
    }

    fun getConversation(uid: String, clickedUid: String, conversationSubject: PublishSubject<Conversation>) {
        // Check if existing conversation with selected and logged in user exists
        val ref = firebaseDatabase.reference.child(CONVERSATIONS)
        ref.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                Log.i(TAG, error!!.message)
            }

            override fun onDataChange(snapshot: DataSnapshot?) {
                if (snapshot!!.hasChildren()) {
                    // Iterate through conversations
                    for (snap in snapshot.children) {
                        val conversation = snap.getValue(Conversation::class.java)
                        conversation.key = snap.key
                        // Check if uid and clickUid match any parameters for conversations
                        // and then return the conversation
                        if (conversation.creatorUid == uid &&
                                conversation.recipientUid == clickedUid) {
                            Log.i(TAG, "Found conversation")
                            conversationSubject.onNext(conversation)
                            conversationSubject.onComplete()
                            break
                        }
                        if (conversation.creatorUid == clickedUid &&
                                conversation.recipientUid == uid) {
                            Log.i(TAG, "Found conversation")
                            conversationSubject.onNext(conversation)
                            conversationSubject.onComplete()
                            break
                        }
                    }
                    // No conversation found, create a new one
                    if (!conversationSubject.hasComplete()) {
                        createConversation(uid, clickedUid, conversationSubject)
                    }
                } else {
                    createConversation(uid, clickedUid, conversationSubject)
                }

            }
        })
    }

    private fun createConversation(uid: String, clickedUid: String,
                                   conversationSubject: PublishSubject<Conversation>) {
        // Create node for messages
        val messageKey = firebaseDatabase.reference.child(MESSAGES).push().key
        val ref = firebaseDatabase.reference.child(CONVERSATIONS)
        val conversation = Conversation(uid, clickedUid, messageKey)
        conversation.creatorOneSignalId = Model.user!!.onesignalUid
        conversation.recipientOneSignalId = Model.selectedUser!!.onesignalUid
        // Get conversation key
        val key = ref.push().key
        conversation.key = key
        // Create node for conversation
        ref.child(key).setValue(conversation).addOnCompleteListener(object: OnCompleteListener<Void> {
            override fun onComplete(task: Task<Void>) {
                Log.i(TAG, "Created conversation: " +task.isSuccessful)
                conversationSubject.onNext(conversation)
                conversationSubject.onComplete()
            }
        })
    }

    fun sendMessage(conversation: Conversation, message: Message,
                    messageSubject: PublishSubject<Boolean>) {
        // Add message to firebase
        var ref = firebaseDatabase.reference.child(MESSAGES)
        ref = ref.child(conversation.messageKey)
        ref.push().setValue(message).addOnCompleteListener(object : OnCompleteListener<Void> {
            override fun onComplete(task: Task<Void>) {
                messageSubject.onNext(task.isSuccessful)
            }
        })
        // Send push notification of message
        if (message.sender == conversation.creatorUid) {
            OneSignalHandler.sendNotification(conversation.recipientOneSignalId, message.text)
        } else {
            OneSignalHandler.sendNotification(conversation.creatorOneSignalId, message.text)
        }

    }

    fun getTypingStatus(conversation: Conversation, userUid: String, typingSubject: PublishSubject<Boolean>,
                        conversationActivity: ConversationActivity) {
        val ref = firebaseDatabase.reference.child(CONVERSATIONS).child(conversation.key)
        val listener = (object : ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                Log.i(TAG, error!!.message)
            }

            override fun onDataChange(snapshot: DataSnapshot?) {
                val convo = snapshot!!.getValue(Conversation::class.java)
                // Decide which user is the device user
                if (convo.creatorUid == userUid) {
                    typingSubject.onNext(convo.recipientTyping)
                } else {
                    typingSubject.onNext(convo.creatorTyping)
                }
            }
        })
        ref.addValueEventListener(listener)
        // Store firebase listener for later removal
        conversationActivity.storeFirebaseTypingListener(listener, ref)
    }

    fun setTypingStatus(conversation: Conversation, userUid: String, isTyping: Boolean) {
        // Check which user is the device user
        if (conversation.creatorUid == userUid) {
            conversation.creatorTyping = isTyping
        } else {
            conversation.recipientTyping = isTyping
        }
        val ref = firebaseDatabase.reference.child(CONVERSATIONS)
                .child(conversation.key)
        ref.setValue(conversation)
    }

    fun getMessages(conversation: Conversation, messagesSubject: PublishSubject<Message>,
                    conversationActivity: ConversationActivity) {
        val ref = firebaseDatabase.reference.child(MESSAGES).child(conversation.messageKey)
        val listener = (object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            // Mainly used for updating read status of messages
            override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {
                val message = snapshot!!.getValue(Message::class.java)
                message.key = snapshot.key
                messagesSubject.onNext(message)
            }

            // Collect all messages
            override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
                val message = snapshot!!.getValue(Message::class.java)
                message.key = snapshot.key
                if (message.sender != Model.user!!.uid) {
                    snapshot.ref.child("status").setValue("Read")
                }
                messagesSubject.onNext(message)

            }

            override fun onChildRemoved(p0: DataSnapshot?) {
            }
        })
        ref.addChildEventListener(listener)
        // Store firebase listener for later removal
        conversationActivity.storeFirebaseMessageListener(listener, ref)
    }

}
