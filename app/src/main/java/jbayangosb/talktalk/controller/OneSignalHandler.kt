package jbayangosb.talktalk.controller

import com.onesignal.OneSignal
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Jayden on 7/4/17.
 */

object OneSignalHandler {
    fun sendNotification(oneSignalId: String, message: String) {
        try {
            OneSignal
                    .postNotification(JSONObject(" {'contents': {'en':'" + message + "'}, " +
                            "'include_player_ids': ['" + oneSignalId + "']}"), null)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}
