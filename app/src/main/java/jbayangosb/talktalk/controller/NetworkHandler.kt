package jbayangosb.talktalk.controller

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by Jayden on 7/4/17.
 */

object NetworkHandler {
    fun isOnline(context: Context): Boolean {
        val connectionManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectionManager.activeNetworkInfo
        return (networkInfo != null && networkInfo.isConnected)
    }
}
